
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;
import utfpr.ct.dainf.if62c.pratica.Quadrado;
import utfpr.ct.dainf.if62c.pratica.Retangulo;
import utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Giovanni Bandeira
 */
public class Pratica43 {

    public static void main() {
        Circulo c = new Circulo(5.);
        Elipse e = new Elipse(3.,7.);
        Retangulo r = new Retangulo(2.,6.);
        Quadrado q = new Quadrado(4.);
        TrianguloEquilatero t = new TrianguloEquilatero(3.);
        System.out.println("Area do Circulo:"+c.getArea());
        System.out.println("Perimetro do Circulo:"+c.getPerimetro());
        System.out.println("Area de elipse:"+e.getArea());
        System.out.println("Perimetro de elipse:"+e.getPerimetro());
        System.out.println("Area de retangulo:"+r.getArea());
        System.out.println("Perimetro de retangulo:"+r.getPerimetro());
        System.out.println("Area de quadrado:"+q.getArea());
        System.out.println("Perimetro de quadrado:"+q.getPerimetro());
        System.out.println("Area de triangulo equilatero:"+t.getArea());
        System.out.println("Perimetro de triangulo equilatero:"+t.getPerimetro());
    }
}
