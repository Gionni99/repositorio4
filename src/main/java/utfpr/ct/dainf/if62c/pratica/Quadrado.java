/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Giovanni Bandeira
 */
public class Quadrado extends Retangulo implements FiguraComLados{
    
    public Quadrado(double lado) {
        super(lado, lado);
    }

    @Override
    public double getArea(){
        return getLadoMenor()*getLadoMaior();
    }
    @Override
    public double getPerimetro(){
        return 2.*getLadoMenor()+2.*getLadoMaior();
    }
}
