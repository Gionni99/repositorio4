/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Giovanni Bandeira
 */
public class Retangulo implements FiguraComLados{
    public double LadoMaior,LadoMenor;
    
    
    public Retangulo(double x, double y) {
        if(x>y){
            this.LadoMaior = x;
            this.LadoMenor = y;
        }
        else{
            this.LadoMaior = y;
            this.LadoMenor = x;
        }
    }
    public double getArea(){
        return getLadoMenor()*getLadoMaior();
    }
    public double getPerimetro(){
        return 2.*getLadoMenor()+2.*getLadoMaior();
    }
    @Override
    public double getLadoMenor() {
        return LadoMenor;
    }

    @Override
    public double getLadoMaior() {
        return LadoMaior;
    }
    
}
